$fs = 1;
base_width		= 51;
top_width		= 42;
depth			= 45;
height			= 10;
base			= 0;
wall_thickness	= 3;
support_diameter	= depth-wall_thickness;
center_hole_diameter= 6;
support_radius		= (support_diameter/2);
center_hole_radius	= center_hole_diameter /2;
module edge_cut (side) { 
	if (side == 1){
		translate ([top_width/2+height/2,0,-.1]) cube ( size = [height+.1,base_width,height+.1], center=true);
	}
	else {
		translate ([-(top_width/2+height/2),0,-.1]) cube ( size = [height+.1,base_width,height+.1], center=true);
	}
}
module mounting_wedge ( ){
	difference(){ 
		translate ([0,0,-height/2]) 
			linear_extrude(height = height, scale = top_width/base_width)
			square(base_width, center = true);
		edge_cut(1);
		edge_cut(2);
	}
}
module center_hole(){
	cylinder ( h=height+.1, r=center_hole_radius, center = true);
}
module bottom_cutout () {
	translate([0,0,-wall_thickness]){
		cube ( size = [top_width-wall_thickness, depth-wall_thickness,height+wall_thickness], center=true);
	}
}
difference(){
	mounting_wedge();
	center_hole();
	bottom_cutout();
}